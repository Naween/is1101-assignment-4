#include<stdio.h>

int main()
{
    const float pi=22.0/7;
    int height, radious;
    float volume;
    printf("Input the height of the cone :");
    scanf("%d",&height);
    printf("Input the radius of the cone :");
    scanf("%d",&radious);
    volume = pi * radious * radious * (float)(height/3);
    printf("Volume of the cone is : %0.3f",volume);
    return 0;
}
